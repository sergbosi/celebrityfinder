package com.celeb.springboot.reactor.app;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.celeb.springboot.reactor.app.models.Person;

import reactor.core.publisher.Flux;

public class DivideAndConquer {

	// Simple log for displayig the data 
	private static final Logger log = LoggerFactory.getLogger(DivideAndConquer.class);
	
	// Function to search an element in
    // minimum number of comparisons
    static Person celebritySearch(List<Person> people, int x)//(Flux<Person> people, int x)
    {
    	log.info("celebritySearch: Start");
    	List<Person> list1 = people;

    	Person last = list1.get(list1.size()-1);
        // 1st comparison
    	log.info("celebritySearch: 1st comparison - lucky attempt");
        if (last.getKnownPeople().size() == x) {
        	log.info("celebritySearch: 1st comparison - lucky attempt Success! Celebrityfound "+last.getName());
            return last;
        } 
        int half = Math.round(list1.size());
        List<Person> list11 = list1.subList(0,  half);
        // this would be executed at-most n times and therefore at-most n comparisons
    	log.info("celebritySearch: searching first half of the collection");
        Person celebrity = failFastSearch( x, list11);
        
        if( celebrity !=null) {
        	return celebrity;
        }else {
        	List<Person> list12 = list1.subList(half, list1.size());
        	log.info("celebritySearch: searching second half of the collection");
        	return failFastSearch( x, list12);         	
        }
    }

    
	private static Person failFastSearch(int x, List<Person> list1) {
		try {
				for (int i = 0;i < list1.size(); i++) {
		          if (list1.get(i).getKnownPeople().size() == x) {
		            	return list1.get(i);
		            }
		         }   
				return null;
		}catch(ArrayIndexOutOfBoundsException exception) {
			log.error(exception.getMessage());
			return null;
		}
	}
}
