package com.celeb.springboot.reactor.app.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="people")
public class Person {
	
	@Id
	private String id;
	private String name;
	private List<Person> knownPeople;
	
	public Person() {
		
	}
	
	public Person(String name) {
		this.name = name;
	}
	
	public Person(String name , List<Person> knownPeople) {
		this.name = name;
		this.knownPeople=knownPeople;
	}
		
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Person> getKnownPeople() {
		return knownPeople;
	}
	public void setKnownPeople(List<? extends Object> list) {
		
		this.knownPeople = (List<Person>) list;
	}
	
	

}
