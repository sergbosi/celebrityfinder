package com.celeb.springboot.reactor.app;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.test.context.junit4.SpringRunner;

import com.celeb.springboot.reactor.app.models.Person;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

 @RunWith(SpringRunner.class)
//@RunWith(JUnitPlatform.class)
 
public class DivideAndConquerTest {
	
 
	 
	private List<Person> people;
	
	@Test
	public void celebritySearchTest() {
		 
		 
		 DivideAndConquer test = new DivideAndConquer();
		 Person p = DivideAndConquer.celebritySearch( people, 0);		 
		 System.out.println(p.getName());
		 assertEquals( "Bruce Lee", p.getName());
		 
		
	}
 
	@Test
	public void celebritySearchFailTest() {
		 
		 DivideAndConquer test = new DivideAndConquer();
		 Person p= test.celebritySearch(people, 1);// returns null when not found
		 
		 assertNull(p);
		 
		
	} 
	
	
 
	
		
	@Before
	public void setUp() {
		people  = new ArrayList<Person>();
		people.add(new Person("andres guzman"));
		people.add(new Person("Pedro Fulano"));
		people.add(new Person("Maria fulana"));
		people.add(new Person("diego arismendi"));
		people.add(new Person("juan Sutano"));	
		people.add(new Person("Jorge gonzo"));
		people.add(new Person("Michael Black"));
		people.add(new Person("Nancy Britto"));
		people.add(new Person("Bruk Willis"));
		people.add(new Person("Bruce Lee"));// celebrity
		
		
		for (Person p: people) {
			if(!p.getName().contains("Lee")) {
				p.setKnownPeople(people);
			}else {
				p.setKnownPeople(new ArrayList());
			}
		}
		
		
	}
	

}
