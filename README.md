**Prerequisites**

- A running MongoDB on localhost using the default port for MongoDB 27017.
- A JVM 1.8 or later
- Maven 3.0 or later


## jar for local test with jvm 

GO to this link and download the spring-boot-reactor-0.0.1-SNAPSHOT.jar [link](https://bitbucket.org/sergbosi/celebrityfinder/src/master/spring-boot-reactor-0.0.1-SNAPSHOT.jar)
 > On windows open a command prompt and run the following command [java -jar spring-boot-reactor-0.0.1-SNAPSHOT.jar]
 

 
